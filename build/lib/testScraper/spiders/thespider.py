# -*- coding: utf-8 -*-
import scrapy
from ..items import TestscraperItem
#from scrapy.utils.response import open_in_browser
from w3lib.http import basic_auth_header
import re


class ThespiderSpider(scrapy.Spider):
    name = 'thespider'
    # start_urls = ["http://www.empresia.es/async/?t=RelacionEntidad&te=empresa&x=65280&offset=580"]
    start_urls = [
        "http://www.empresia.es/async/?t=RelacionEntidad&te=empresa&x=65280&offset={}".format(i) for i in range(0, 581, 20)
        ]

    # for link in start_urls:
    #     yield scrapy.Request(url=link, callback=self.parse)
    def start_requests(self):
        for URL in self.start_urls:
            yield scrapy.Request(url=URL, method='POST', callback=self.parse)

    def parse(self, response):
        # basic_auth_header(response)
        items = TestscraperItem()

        all_data = response.css('::text').extract()
        all_data = list(filter(lambda element: element != ' ', all_data))

        sinceDate = False   # Flag to indicate since date is reached
        loop = 0            # flag to indicate loop number
        for element in all_data:
            if 'Ver más' in element:
                yield items
                break

            elif loop == 1:
                items['relationship'] = element
                loop = 0

            elif re.match('\D\D', element):
                loop += 1
                if sinceDate:
                    yield items

                items['entity'] = element
                sinceDate = False

            elif sinceDate:
                items['until'] = element


            else:
                items['since'] = element
                try:
                    del items['until']
                except KeyError:
                    pass

                sinceDate = True

            if all_data[-1] == element and element != 'Ver más...':
                yield items
